Role Name
=========

This role creates deploy user.

Role Variables
--------------

Set needed parameters in vars/main.yml.


Example Playbook
----------------


    - hosts: servers
      roles:
         - create_deploy_user

License
-------

GNU/GPL

Author Information
------------------

Gabriel Greslik
