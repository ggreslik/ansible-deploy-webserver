Role Name
=========

This role starts container and publishes exposed port.

Requirements
------------

1. Created deploy user
2. Installed and running docker-ce.
3. Built image

Role Variables
--------------

Role variables are defined in vars/main.yml.

Dependencies
------------

Roles:
    - create-deploy-user
    - install-docker
    - create-dockerfile
    - build-docker-image


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - role: run-docker-container
          become: yes
          become_user: deploy

License
-------

GNU/GPL

Author Information
------------------

Gabriel Greslik
