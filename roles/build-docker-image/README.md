Role Name
=========

This playbook builds docker image in defined directory

Requirements
------------

1. Connection to internet to pull base image if not locally present
2. Existing deploy user
3. Installed and running docker

Role Variables
--------------

Role variables are defined in vars/main.yml.

Dependencies
------------

Roles:
    - create-deploy-user
    - install-docker
    - create-dockerfile

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - role: build-docker-image
          become: yes
          become_user: deploy

License
-------

GNU/GPL

Author Information
------------------

Gabriel Greslik
