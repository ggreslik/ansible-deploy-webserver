Role Name
=========

This role create Dockerfile and put it together with web file to build directory.

Requirements
------------


Role Variables
--------------

Variables are defined in vars/main.yml.

Dependencies
------------

Roles:
create_deploy_user
install_docker

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - create-dockerfile
          become: yes
          become_user: deploy
License
-------

GNU/GPL
Author Information
------------------

Gabriel Greslik
