Role Name
=========

This role installs docker on CentOS.

Requirements
------------

1. Connection to Internet
2. CentOS 7,8 (CentOS Linux 6 does not have kernel with docker support) 

--------------

Dependencies
------------

Role: create_deploy_user

Example Playbook
----------------

    - hosts: servers
      roles:
         - install_docker

License
-------

GNU/GPL

Author Information
------------------
Gabriel Greslik
