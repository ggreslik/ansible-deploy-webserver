# ansible-deploy-webserver

## Description
This project is for automatic deployment of nginx web server on **CentOS**. The web server is running in docker container. The deployment is done by **Ansible**.

## Ansible Installation
Please follow the [installation instructions](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
Additionaly is needed [ansible community docker](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_container_module.html#ansible-collections-community-docker-docker-container-module).

## Ansible Playbook Tasks

1. Create user deploy
2. Install docker
3. Create Dockerfile for building docker image with sample static web page 
4. Build docker container
5. Run docker container

## Usage
* You can change image, container name and port in **vars/webserver_vars.yml**
* Webserver **index.html** can be changed in **roles/create-dockerfile/files/**. If you add more files you need to adapt Dockerfile to copy them too
* You can use your inventory or adapt inventory file in the project

Example run:
_ansible-playbook -i inventory deploy_web_server.yml_

## Authors and acknowledgment
Gabriel Greslik

## License
GNU GPL

## Project status
This is test project at the moment.

